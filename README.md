# HC-SR04 Ultra Sonic Range Detector and the MSP430

## By: Ren Herbert

This file is set up to run the HC-SR04 Ultra Sonic Range Detector. It is assumed that the ECHO signal of the US Sensor will be connected to P2.4 and the trigger will be connected to P1.4. Before the check_US() function can be ran, the initUSonMSP430() function must be called. Once the init function has been called once, it does not need to be called again. The check function returns the number of cycles that the current clock configuration used while the ECHO signal was high. This means that some conversion is necessary to get a direct distance: every microsecond the signal was high is 1/58 centimeters or 1/148 inches.