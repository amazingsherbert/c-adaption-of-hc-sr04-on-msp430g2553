/*
 * UltraSonic.c
 *
 *  Created on: Dec 4, 2015
 *      Author: Ren Herbert
 *
 * README: This file is set up to run the HC-SR04 Ultra Sonic Range Detector. It is assumed that the ECHO signal of the US Sensor will be
 * connected to P2.4 and the trigger will be conncted to P1.4. Before the check_US() function can be ran, the initUSonMSP430() function must be called.
 * once the init function has been called once, it does not need to be called again. The check function returns the number of cycles that the current
 * clock configuration used while the ECHO signal was high. This means that some conversion is necessary to get a direct distance: every microsecond
 * the signal was high is 1/58 centimeters or 1/148 inches.
 */

#include "UltraSonic.h"

long check_US(void)
{
	/*
	 *
	 * Return the number of cycles used for response from US.
	 * Inputs: NONE
	 * Return: (long)Num_of_cycles
	 *
	 */
	P1OUT |= BIT4;
	__delay_cycles(850); //send 10us pulse to trigger the US Sensor
	P1OUT &= ~BIT4;


	P2IN = 0;
	int final_time;
	int start;
	count = 0;
	int16 length;
	while((P2IN & BIT4) == 0){} // wait for signal to rise
	start = TAR;
	multiplier = 0;
	while((P2IN & BIT4) == 16){} // wait for signal to fall
	final_time = TAR;
	length = (final_time - start) + (int)(TA0CCR0) * multiplier;
	return length;
}

void initUSonMSP430(void)
{
	WDTCTL=WDTPW+WDTHOLD; 				// stop WD

	BCSCTL1 = CALBC1_8MHZ;
	DCOCTL = CALDCO_8MHZ;

	P2DIR &= ~BIT4;						// Connect Echo to P2.4
	P2SEL &= ~BIT4;
	P2SEL2 &= ~BIT4;

	TA0CCR0	= 5000;					// create a 16ms roll-over period
	TACTL &= ~TAIFG;					// clear flag before enabling interrupts = good practice
	TACTL |= ID_3|TASSEL_2|MC_1|TAIE;	// Use 1:8 prescalar off SMCLK and enable interrupts

	_enable_interrupt();

    P1DIR |= BIT4;					// P1.4 is the triggering pin

    P2DIR &= ~BIT4;					// P2.4 set up as input to recieve the Echo signal
    P2IN  &= ~BIT4;
}



